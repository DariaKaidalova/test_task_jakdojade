# About the project

## How to launch the project:
- In the folder `project` install project using command `npm install`
- Run the project using command `ng serve`
- Open the link `http://localhost:4200/` in the browther.

## Performed tasks:

- I created pages using Angular 8, html markup and scss
- On the main page there is a list of bike stations with the following information: station name, distance to the station, available bikes and places. They are sorted by distance.
- To go into the page with station detail information click on the particular station in the list. There is a map and the following information: station name, distance to the station, available bikes and places.

## Not performed tasks:

- Bike station and current location markers. The reason: I don't have api key to use google map, thus I used google map iframe because it is free and thus I cannot pass custom icon for marker.
- Bike station address. The same reason.


