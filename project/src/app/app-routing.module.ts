import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StationsListComponent } from './components/stations-list/stations-list.component';
import { StationDetailsComponent } from './components/station-details/station-details.component';

const routes: Routes = [
  { path: '', component:   StationsListComponent },
  { path: 'station/:id', component:  StationDetailsComponent },
  { path: '', redirectTo: '/', pathMatch: 'full'},
  { path: '**', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
