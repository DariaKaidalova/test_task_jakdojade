import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

/* routing */
import { AppRoutingModule } from './app-routing.module';

/* services */
import { FilterService } from './services/filter.service'
import { RestService } from './services/rest.service'
import { LocationService } from './services/location.service'

/* components */
import { AppComponent } from './app.component';
import { ContentComponent } from './components/content/content.component';
import { StationComponent } from './components/station/station.component';
import { StationsListComponent } from './components/stations-list/stations-list.component';
import { HeaderComponent } from './components/header/header.component';
import { AvailibilityComponent } from './components/availibility/availibility.component';
import { StationDetailsComponent } from './components/station-details/station-details.component';
import { MapComponent } from './components/map/map.component';
import { SafeUrlPipe } from './pipes/safe-url.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    StationComponent,
    StationsListComponent,
    HeaderComponent,
    AvailibilityComponent,
    StationDetailsComponent,
    MapComponent,
    SafeUrlPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    FilterService,
    RestService,
    LocationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
