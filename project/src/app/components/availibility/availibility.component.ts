import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-availibility',
  templateUrl: './availibility.component.html',
  styleUrls: ['./availibility.component.scss']
})
export class AvailibilityComponent implements OnInit {
  @Input() public type: string = 'bikes';
  @Input() public title: string = '';
  @Input() public numbers: number = 0;

  constructor() { }

  ngOnInit() {
  }

}
