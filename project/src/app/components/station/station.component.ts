import { Component, OnInit, Input } from '@angular/core';
import { StationModel } from 'src/app/models/station-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.scss']
})
export class StationComponent implements OnInit {
  @Input() public station: StationModel;
  @Input() public type: string = 'list';

  constructor(private _router: Router) {}

  ngOnInit() {}

  goToDEtailsPage(id) {
    id = id ? id : null;
    if(this.type === 'list') {
      this._router.navigate(['/station', id]);
    }
  }

}
