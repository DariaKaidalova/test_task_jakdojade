import { Component, OnInit } from '@angular/core';
import { pluck, switchMap, takeWhile } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { RestService } from 'src/app/services/rest.service';
import { StationModel } from 'src/app/models/station-model';

@Component({
  selector: 'app-station-details',
  templateUrl: './station-details.component.html',
  styleUrls: ['./station-details.component.scss']
})
export class StationDetailsComponent implements OnInit {
  public station: StationModel;
  public distance: number = 0;
  private sub: boolean = true;

  constructor(
    private _restService: RestService,
    private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this._activatedRoute.params.pipe(

      pluck('id'),
      switchMap(id => this._restService.getStation(id)),
      takeWhile( () => this.sub)

    ).subscribe(station => {

      this.station = station;

    });
  }

  ngOnDestroy(): void {

    this.sub = false;

  }

}
