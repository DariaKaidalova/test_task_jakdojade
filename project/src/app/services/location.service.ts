import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor() { }

  public getDistance(lat1: number, lon1: number, lat2: number, lon2: number): number {
    var R = 6371; // Radius of the earth in km
    var dLat = this.convertDegreesToRadians(lat2 - lat1);
    var dLon = this.convertDegreesToRadians(lon2 - lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.convertDegreesToRadians(lat1)) * Math.cos(this.convertDegreesToRadians(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2);

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d =  Math.round(R * c * 1000); // Distance in m
    return d;
  }
  
  private convertDegreesToRadians(deg: number): number {
    return deg * (Math.PI/180)
  }

  public getPosition(): Observable<any> {
    return from(new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(response => {
        resolve({
            lon: response.coords.longitude, 
            lat: response.coords.latitude
          });
        },
        error => {
          resolve(null);
          console.warn(error);
        });

    }));
  }
}
