import { GeometryModel } from './geometry-model';
import { PropertiesModel } from './properties-model';

export interface StationInterface {

  id: string;
  geometry: GeometryModel;
  properties: PropertiesModel;
  type: string;
  distance?: number;
  
}
