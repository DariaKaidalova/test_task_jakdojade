export interface GeometryInterface {

  coordinates: [number, number];
  type: string;
  
}
