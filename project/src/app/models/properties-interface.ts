export interface PropertiesInterface {

  bike_racks: string | number;
  bikes: string | number;
  free_racks: string | number;
  label: string | number;
  updated: string;
  
}
