import { PropertiesInterface } from './properties-interface';

export class PropertiesModel {

  bike_racks: string | number;
  bikes: string | number;
  free_racks: string | number;
  label: string | number;
  updated: string;

  constructor(opts: Partial<PropertiesInterface>) {
    
    this.bike_racks = opts.bike_racks;
    this.bikes = opts.bikes;
    this.free_racks = opts.free_racks;
    this.label = opts.label;
    this.updated = opts.updated;

  }
  
}
